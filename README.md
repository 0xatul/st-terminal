`sudo make install` to build and copy it to `/usr/bin`
Bells and whistles:
* Implemented vimbindings:
You can use `alt+k/pageup` to scroll up and `alt+j/pagedown`
* Added xresources patch: 
You dont have to recompile just to change the color scheme.
* Follow urls with dmenu:
Press `alt+l` to get a dmenu popup which will let you follow anyurl in it 
